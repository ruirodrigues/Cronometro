﻿namespace Cronometro
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private readonly Cronometro _cronometro;

        public Form1()
        {
            InitializeComponent();
            _cronometro = new Cronometro();
        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            if (_cronometro.ClockState())
            {
                _cronometro.StopClock();
                ButtonOnOff.Text = "Liga";
                TimerRelogio.Enabled = false;
                //LabelContador.Text = _cronometro.GetTimeSpan().ToString();
            }
            else
            {
                _cronometro.StartClock();
                ButtonOnOff.Text = "Desliga";
                TimerRelogio.Enabled = true;
            }
        }

        private void UpdateLabel()
        {
            var tempo = DateTime.Now - _cronometro.StartTime();
            LabelContador.Text = string.Format($"{tempo.Hours:d2}:{tempo.Minutes:d2}:{tempo.Seconds:d2}:{tempo.Milliseconds:d3}");
        }

        private void TimerRelogio_Tick(object sender, EventArgs e)
        {
            UpdateLabel();
        }
    }
}
