﻿namespace ConsoleCronometro
{
    using System;
    using Cronometro;

    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cronometro();
            Console.WriteLine("Pressione Enter para iniciar o cronómetro");
            Console.ReadLine();
            relogio.StartClock();

            Console.WriteLine("Pressione Enter novamente para parar o cronómetro");

            while (relogio.ClockState())
            {
                var tempo_corrente = DateTime.Now - relogio.StartTime();
                //Console.Write($"\rTempo corrente: {tempo}");
                Console.Write($"\r Tempo corrente: {tempo_corrente.Hours:d2}:{tempo_corrente.Minutes:d2}:{tempo_corrente.Seconds:d2}:{tempo_corrente.Milliseconds:d2}");

                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().Key == ConsoleKey.Enter)
                    {
                        relogio.SetRunning(false);
                    }
                }
            }

            relogio.SetRunning(true); // _isrunning tem que voltar a ser verdade para não termos excepção no método StopClock
            relogio.StopClock();

            var tempo_cronometrado = relogio.GetTimeSpan();
            Console.WriteLine($"\r Tempo Cronometrado: {tempo_cronometrado.Hours:d2}:{tempo_cronometrado.Minutes:d2}:{tempo_cronometrado.Seconds:d2}:{tempo_cronometrado.Milliseconds:d2}");

            Console.ReadKey();
        }
    }
}
